from icalendar import Calendar, Event, vCalAddress, vText;
import pytz;
from datetime import datetime, date, timedelta;
from calendar import monthrange, weekday;
import os;
from pathlib import Path;

from getpass import getpass;
import oracledb

# appel au fichier parametres.py
from parametres import *


def main():
    current_year = date.today().year
    # ou bien commenter (#) la ligne précédente et indiquer manuellement l'année, par exemple : (enlever le # de la ligne suivante)
    #current_year = 2024
    pw = getpass(f"Mdp {REPO_GENIO['user']} : ")
    oracledb.init_oracle_client(lib_dir=r"D:\instantclient\instantclient_19_22")
    with oracledb.connect(user=REPO_GENIO['user'], password=pw, host=REPO_GENIO['host'],port=1521,service_name=REPO_GENIO['service_name']) as connection:
        print("connecté")
        with connection.cursor() as cursor:
        # Les événements périodiques :
            tab = []
            for r in cursor.execute(f"""
            select s_program.s_prgid, g_fat.g_objid, g_fat.g_objname,prjname, foldername,
            decode(s_prgperiod, 0, 'Quotidien', 1, 'Hebdomadaire', 2, 'Mensuel_D', 4, 'Mensuel_JS') frequence,
            s_prgperiodparam, to_char(s_prgdatetime, 'HH24:MI:SS') heure, cast(percentile_75 as int) percentile_75
            from s_program 
            inner join g_fat on s_program.g_prsid = g_fat.g_objid and g_fat.g_objtype = 22
            inner join genio_analyse.g_fat_process gfp on gfp.g_objid = g_fat.g_objid and gfp.g_objtype = g_fat.g_objtype
            left outer join (SELECT s_prgid, g_objid,
                percentile_cont (0.75) WITHIN GROUP
                (ORDER BY (s_logdtend-s_logdtstart )*24*60*60 ASC)  as percentile_75
                FROM s_log where s_prgid <> -1 and g_objtype = 22 group by s_prgid, g_objid ) percentile
                on percentile.s_prgid = s_program.s_prgid and percentile.g_objid = s_program.g_prsid
            where s_prgstatus = 0 and s_prgtype = 1
            """):
                tab.append(r)


    cal = Calendar()
    cal.add('attendee', 'MAILTO:abc@example.com')
    cal.add('attendee', 'MAILTO:xyz@example.com')

    organizer = vCalAddress('MAILTO:genio@opentext.org')
    organizer.params['cn'] = vText('Genio')
    vm_name = vText(SERV_GENIO)


    for prg in tab:
        process_name, project_name, folder_name, period_param, hour, duration = prg[2],prg[3],prg[4], prg[6],prg[7],prg[8]
###             ###
### Quotidien   ###
###             ###
        if prg[5] == "Quotidien":
            start_date = datetime(current_year,1,1,int(hour[:2]),int(hour[3:5]), int(hour[6:8]) )
            while start_date.year != current_year+1:
                add_event_to_calendar(cal, process_name, project_name, folder_name, start_date, duration,organizer, vm_name)
                
                start_date = start_date + timedelta(days=1)

###              ###
### Hebdomadaire ###
###              ###  
      
        elif prg[5] == "Hebdomadaire":

            # quel jour trouve-t-on le premier period_param de janvier ? (par exemple, le premier lundi de janvier 2024 : 1er janvier)
            day_number = first_weekday_to_day(current_year, period_param, month=1)

            start_date = datetime(current_year,1,day_number,int(hour[:2]),int(hour[3:5]), int(hour[6:8]) )
            
            while start_date.year != current_year+1:
                add_event_to_calendar(cal, process_name, project_name, folder_name, start_date, duration,organizer, vm_name)
                
                start_date = start_date + timedelta(days=7)

                    
###           ###
### Mensuel_D ###
###           ###                     
# le paramètre period_param précise le jour du mois (ex: tous les 13 du mois)

        elif prg[5] == "Mensuel_D":
            # on boucle sur chaque mois :
            for month in range(1,13):
                start_date = datetime(current_year, month, period_param, int(hour[:2]),int(hour[3:5]), int(hour[6:8]) )
                add_event_to_calendar(cal, process_name, project_name, folder_name, start_date, duration,organizer, vm_name)
      
###            ###
### Mensuel_JS ###
###            ###  

        elif prg[5] == "Mensuel_JS":
            # on boucle sur chaque mois : 
                for month in range(1,13):
                    day_number = first_weekday_to_day(current_year, period_param, month)
                    start_date = datetime(current_year, month, day_number, int(hour[:2]),int(hour[3:5]), int(hour[6:8]) )
                    add_event_to_calendar(cal, process_name, project_name, folder_name, start_date, duration,organizer, vm_name)



        
    # Les événements programmés une fois 

    # Les événements avec trigger



    directory = "D://"
    directory = "D://"
    print("ics file will be generated at ", directory)
    f = open(os.path.join(directory, 'genio.ics'), 'wb')
    f.write(cal.to_ical())
    f.close()




# Fonctions :
def create_event(process_name, project_name, folder_name, start_date, duration, organizer, vm_name):
    end_date = start_date + timedelta(0,duration)
    event = Event()
    event.add('summary',process_name) 
    event.add('dtstart',start_date) 
    event.add('dtend',end_date)
    event.add('description', f"VM : {vm_name} \n project name : {project_name} \n folder name : {folder_name}")
    event['organizer'] = organizer
    event['location'] = project_name
    return event
    

def add_event_to_calendar(cal, process_name, project_name, folder_name, start_date, duration,organizer, vm_name):
    event = create_event(process_name, project_name, folder_name, start_date, duration,organizer, vm_name)
    cal.add_component(event)
    
def first_weekday_to_day(current_year,period_param, month):
    """
    Entrée : 
        - period_param : jour de la semaine renvoyé par Genio auquel un événement est prévu 
        - mois pour lequel on cherche le premier period_param
    Sortie : 
        - day_number : le jour (de 1 à 7) pour lequel le jour de semaine est bien period_param
    """
    # On traduit la numérotation des jours de la semaine de Genio dans celle du module calendar de Python
    # pour genio, dimanche = 1, alors que pour calendar, lundi=0 soit dimanche = 6 
    period_param = (period_param + 5) % 7

    #quel est le jour de semaine du 1er "month" de l'année "current_year" ?
    week_day = monthrange(current_year,month)[0]
    #si week_day correspond à period_param, alors on retourne le premier jour du mois
    day_number = 1
    #combien de jours faut-il rajouter pour trouver le jour de semaine donné par le period_param calculé ci-dessus ?
    while (week_day+0) != period_param:
        day_number += 1
        week_day = weekday(current_year, month, day_number)
    
    return day_number

   
if __name__ == '__main__':
    main()